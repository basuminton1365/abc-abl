# -*- coding:utf-8 -*-
import tkinter as tk
import tkinter.ttk as ttk
import sqlite3

#登録画面のGUI
def create_gui():
    #----------------------------------
    #コールバック関数群
    #-----------------------------------
    #検索ボタンが押されたときのコールバック関数
    def select_button():
        abc.destroy()
        select_gui()
    #-------------------------------------
    #終了ボタンが押下されたときにアプリを終了する
    def qui_button():
        abc.destroy()          
    #---------------------------------------
    # 登録ボタンがクリックされたときにdataをdbに登録するコールバック関数 
    
    def create_sql():
        c = sqlite3.connect("database.db")
        # 言語の読み取り
        item_language = text_1.get()
        # 内容の読み取り
        item_code = text_2.get()
        # 説明の読み取り
        item_name = text_3.get()
        #メモの読み取り
        item_text = text_4.get()
        # SQLを発行してDBへ登録
        try:
            #c.execute("INSERT INTO a VALUES('{}','{}','{}','リストの長さを取得');")
            c.execute("""
            INSERT INTO dics(item_language,item_code,item_name,item_text)
            VALUES("{}","{}","{}","{}");
            """.format(item_language,item_code,item_name,item_text))
            c.execute("COMMIT;")
            print("1件登録しました")
        # ドメインエラーなどにより登録できなかった場合のエラー処理
        except:
            print("エラーにより登録できませんでした")
            
            
    #--------------------------------------------------
    dbname = "database.db"
    c = sqlite3.connect(dbname)
    c.execute("PRAGMA foreign_keys = 1")

    try:
        ddl = """
        CREATE TABLE dics
        (
            item_language INTEGER,
            item_code INTEGER,
            item_name INTEGER,
            item_text INTEGER
        );
        """
        c.execute(ddl)
    except:
        print("a")
        pass

    abc = tk.Tk() #Tkオブジェクト生成
    abc.title(u"ABC-ABL") #タイトル
    abc.geometry("400x400") #サイズ指定

    #フレーム作成(フレーム淵と形状を指定)
    frame = tk.Frame(abc,bd=2,relief="ridge")
    frame.pack(fill = "x")
    #上部切り替え用ボタン作成・配置
    kensakuButton = tk.Button(frame,text="検索",command=select_button)
    kensakuButton.pack(side="left")
    tourokuButton = tk.Button(frame,text="登録")
    tourokuButton.pack(side="left")
    owariButton = tk.Button(frame,text="終了",command=qui_button)
    owariButton.pack(side="right")

    #検索メニュー作成↓↓↓
    frame1 = tk.Frame(abc,pady=10,bg="gray")
    #部品・ラベルを作成
    label = tk.Label(abc,text = "【登録画面】",font=("",20),height=2)
    label2 = tk.Label(abc,text = "--言語--",font=("",16))
    label3 = tk.Label(abc,text = "--種類--",font=("",16))
    label4 = tk.Label(abc,text = "--内容--",font=("",16))
    label5 = tk.Label(abc,text = "--説明--",font=("",16))
    #テキストボックス(エントリー)作成
    text_1 = tk.Entry(abc,font=("",12),justify="center",width=15)
    text_2 = tk.Entry(abc,font=("",12),justify="center",width=15)
    text_3 = tk.Entry(abc,font=("",12),justify="center",width=18)
    text_4 = tk.Entry(abc,font=("",12),justify="center",width=18)
    label.pack() #順番に配置
    frame1.pack()
    label2.pack()
    frame1.pack()
    text_1.pack()
    frame1.pack()
    label3.pack()
    frame1.pack()
    text_2.pack()
    label4.pack()
    text_3.pack()
    label5.pack()
    text_4.pack()
    #登録ボタン設定
    searchButton = tk.Button(abc,text="登録",font=("",15),width=10,bg="gray",command=create_sql)
    searchButton.pack()

    abc.mainloop()



#検索画面のGUI
def select_gui():
    #------------------------------------------------
    #コールバック関数群
    #------------------------------------------------
    #登録ボタンが押されたとき
    def create_button():
        abc.destroy()
        create_gui()
    #------------------------------------------------
    #終了ボタンが押されたとき
    def qui_button():
        abc.destroy()    
    #------------------------------------------------
    #検索ボタンが押されたとき
    def select_button(word):
        # treeviewのアイテムをすべて削除
        tree.delete(*tree.get_children())
        # 空欄だったらデフォルト値の設定
        if word == "":
            word = "python"
        
        sql = """
        SELECT item_language,item_code,item_name,item_text
        FROM dics
        WHERE item_language LIKE '{}' OR
        item_code LIKE '{}' OR
        item_name LIKE '{}' OR
        item_text LIKE '{}' 
        """.format( word +'%',word + '%',word + '%',word + '%')
        # ツリービューにアイテムの追加
        i=0
        for r in c.execute(sql):
            tree.insert("","end",tags=i,values=r)
            if i & 1:
                tree.tag_configure(i,background="#CCFFFF")
            i+=1 
    #------------------------------------------------
    dbname = "database.db"
    c = sqlite3.connect(dbname)
    c.execute("PRAGMA foreign_keys = 1")

    abc = tk.Tk() #Tkオブジェクト生成
    abc.title(u"ABC-ABL") #タイトル
    abc.geometry("700x500") #サイズ指定

    #メニュー設定
    frame = tk.Frame(abc,bd=2,relief="ridge")
    frame.pack(fill="x")
    button1 = tk.Button(frame,text="検索")
    button1.pack(side="left")
    button2 = tk.Button(frame,text="登録",command=create_button)
    button2.pack(side="left")
    button3 = tk.Button(frame,text="終了",command=qui_button)
    button3.pack(side="right")


    # 検索画面ラベルの設定
    label = tk.Label(abc,text = "【検索画面】",font=("",20),height=2)
    #label2 = tk.Label(abc,text = "--言語--",font=("",16))
    label3 = tk.Label(abc,text = "--キーワード--",font=("",16))
    #テキストボックス(エントリー)作成
    #text_1 = tk.Entry(abc,font=("",12),justify="center",width=15)
    text_2 = tk.Entry(abc,font=("",12),justify="center",width=15)
    label.pack() #順番に配置
    #label2.pack()
    #text_1.pack()
    label3.pack()
    text_2.pack()
    #検索ボタン設定
    searchButton = tk.Button(abc,text="検索",font=("",15),width=10,bg="gray",command=lambda:select_button(text_2.get()))
    searchButton.pack()


    #ツリービュー作成
    tree = ttk.Treeview(abc,padding=10)
    # 列インデックスの作成
    tree["columns"] = (1,2,3,4)
    #表スタイルの設定
    tree["show"] = "headings"
    # 各列の設定(インデックス,オプション(今回は幅を指定))
    tree.column(1,width=75)
    tree.column(2,width=75)
    tree.column(3,width=100)
    tree.column(4,width=100)
    # 各列のヘッダー設定(インデックス,テキスト)
    tree.heading(1,text="言語")
    tree.heading(2,text="種類")
    tree.heading(3,text="内容")
    tree.heading(4,text="説明")

    # ツリービューのスタイル変更
    style = ttk.Style()
    # TreeViewの全部に対して、フォントサイズの変更
    style.configure("Treeview",font=("",12))
    # TreeViewのHeading部分に対して、フォントサイズの変更と太字の設定
    style.configure("Treeview.Heading",font=("",14,"bold"))


    # SELECT文の作成
    sql = """
    SELECT item_language,item_code,item_name,item_text
    FROM dics
    """

    # SELECT文で取得した各レコードを繰り返し取得
    i = 0
    for r in c.execute(sql):
        # ツリービューの要素に追加
        tree.insert("","end",tags=i,values=r)
        if i & 1:
            tree.tag_configure(i,background="#CCFFFF")
        i += 1
    # ツリービューの配置
    tree.pack(fill="x",padx=20,pady=20)

    abc.mainloop()


# このスクリプトが実行されたときに最初に登録画面を表示する
create_gui()

